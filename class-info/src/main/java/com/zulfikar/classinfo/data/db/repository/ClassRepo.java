package com.zulfikar.classinfo.data.db.repository;

import com.zulfikar.classinfo.data.db.entity.Class;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepo extends JpaRepository<Class, String> {
}