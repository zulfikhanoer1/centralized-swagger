package com.zulfikar.classinfo.data.db.repository;

import com.zulfikar.classinfo.data.db.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<Student, String> {
    @Query(value = "SELECT count(*) FROM public.student s WHERE :id = ANY(s.\"ACTIVE_CLASS\")", nativeQuery = true)
    int countRegisteredStudents(String id);
}
