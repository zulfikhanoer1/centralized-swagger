package com.zulfikar.classinfo.data.db.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "class", schema = "public")
@Setter
@Getter
public class Class {

  @Id
  @Column(name = "\"ID\"")
  private String id;
  @Column(name = "\"SUBJECT\"")
  private String subject;
  @Column(name = "\"GRADE\"")
  private long grade;
}
