package com.zulfikar.classinfo.presenter.rest.api;

import com.zulfikar.classinfo.core.domain.ClassDetailRequest;
import com.zulfikar.classinfo.core.domain.ClassResponse;
import com.zulfikar.classinfo.core.useccases.ClassUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/class")
@RequiredArgsConstructor
public class ClassInfoController {

    final ClassUseCase classUseCase;

    @GetMapping("/list")
    ResponseEntity<ClassResponse> getList() {
        return classUseCase.getList();
    }

    @PostMapping("/detail")
    ResponseEntity<ClassResponse> getDetail(@RequestBody ClassDetailRequest request) {
        return classUseCase.getDetail(request);
    }
}
