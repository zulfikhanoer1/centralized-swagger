package com.zulfikar.classinfo.core.useccases;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zulfikar.classinfo.core.domain.ClassDetailRequest;
import com.zulfikar.classinfo.core.domain.ClassResponse;
import com.zulfikar.classinfo.data.db.entity.Class;
import com.zulfikar.classinfo.data.db.entity.Student;
import com.zulfikar.classinfo.data.db.repository.ClassRepo;
import com.zulfikar.classinfo.data.db.repository.StudentRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClassUseCase {

    final ClassRepo classRepo;
    final StudentRepo studentRepo;
    ObjectMapper objectMapper = new ObjectMapper();

    public ResponseEntity<ClassResponse> getList() {
        ClassResponse classResponse = new ClassResponse();
        try {
            List<Class> classDetail = classRepo.findAll();
            classResponse.setResponseCode("00");
            classResponse.setResponseDesc("Success");
            classResponse.setData(classDetail);
            return ResponseEntity.status(HttpStatus.OK).body(classResponse);
        } catch (Exception e) {
            log.error("caught Error :", e);
            classResponse.setResponseCode("500");
            classResponse.setResponseDesc("General Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(classResponse);
        }
    }

    public ResponseEntity<ClassResponse> getDetail(ClassDetailRequest request) {
        ClassResponse classResponse = new ClassResponse();
        String id = request.getId();
        try {
            Optional<Class> classDetail = classRepo.findById(id);
            if (classDetail.isEmpty()) {
                classResponse.setResponseCode("404");
                classResponse.setResponseDesc("Not Found Data With ID " +id);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(classResponse);
            }
            int studentCount = studentRepo.countRegisteredStudents(id);
            LinkedHashMap<String, Object> data  = objectMapper.convertValue(classDetail.get(), LinkedHashMap.class);
            data.put("registered students", studentCount);

            classResponse.setResponseCode("00");
            classResponse.setResponseDesc("Success");
            classResponse.setData(data);
            return ResponseEntity.status(HttpStatus.OK).body(classResponse);
        } catch (Exception e) {
            log.error("caught Error :", e);
            classResponse.setResponseCode("500");
            classResponse.setResponseDesc("General Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(classResponse);
        }
    }
}
