package com.zulfikar.classinfo.core.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClassResponse {
    String responseCode;
    String responseDesc;
    Object data;
}
