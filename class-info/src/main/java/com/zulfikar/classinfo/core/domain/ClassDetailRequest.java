package com.zulfikar.classinfo.core.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClassDetailRequest {
    String id;
}
