create table class
(
    "ID"      varchar(10)  not null
        primary key,
    "SUBJECT" varchar(255) not null,
    "GRADE"   smallint     not null
);

alter table class
    owner to postgres;


INSERT INTO public.class ("ID", "SUBJECT", "GRADE") VALUES ('MATH-01', 'MATH', 1);
INSERT INTO public.class ("ID", "SUBJECT", "GRADE") VALUES ('BIOLOGY-01', 'BIOLOGY', 1);
INSERT INTO public.class ("ID", "SUBJECT", "GRADE") VALUES ('MATH-02', 'MATH', 2);
INSERT INTO public.class ("ID", "SUBJECT", "GRADE") VALUES ('ART-01', 'ART', 2);

create table student
(
    "ID"           uuid default gen_random_uuid() not null
        primary key,
    "NAME"         varchar(255)                   not null,
    "GRADE"        smallint                       not null,
    "ACTIVE_CLASS" character varying[]            not null
);

alter table student
    owner to postgres;

INSERT INTO public.student ("ID", "NAME", "GRADE", "ACTIVE_CLASS") VALUES ('8aa01c5f-cc4b-4c0c-af01-4b083daa356d', 'Samuel', 1, '{MATH-01,BIOLOGY-01}');
INSERT INTO public.student ("ID", "NAME", "GRADE", "ACTIVE_CLASS") VALUES ('af7cfb70-9d79-4a24-92cf-f6df6a9ef749', 'Johnny', 1, '{MATH-01}');
INSERT INTO public.student ("ID", "NAME", "GRADE", "ACTIVE_CLASS") VALUES ('da21aa37-ccf9-4a68-b32a-f8716881fad1', 'Fred', 2, '{MATH-01,MATH-02}');
INSERT INTO public.student ("ID", "NAME", "GRADE", "ACTIVE_CLASS") VALUES ('62527698-0b9d-48aa-b8ba-1fd6376c6404', 'Solomon', 2, '{MATH-02,ART-02}');