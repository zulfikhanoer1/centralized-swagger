package com.zulfikar.studentinfo.data.db.repository;

import com.zulfikar.studentinfo.data.db.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<Student, String> {
    List<Student> findByName(String name);
}
