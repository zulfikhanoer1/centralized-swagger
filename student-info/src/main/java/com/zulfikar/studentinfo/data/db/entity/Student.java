package com.zulfikar.studentinfo.data.db.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "student", schema = "public")
@Setter
@Getter
public class Student {

  @Id
  @Column(name = "\"ID\"")
  private String id;
  @Column(name = "\"NAME\"")
  private String name;
  @Column(name = "\"GRADE\"")
  private long grade;
  @Column(name = "\"ACTIVE_CLASS\"")
  private String activeClass;
}
