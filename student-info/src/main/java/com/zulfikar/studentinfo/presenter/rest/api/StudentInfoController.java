package com.zulfikar.studentinfo.presenter.rest.api;

import com.zulfikar.studentinfo.core.domain.StudentDetailRequest;
import com.zulfikar.studentinfo.core.domain.StudentResponse;
import com.zulfikar.studentinfo.core.useccases.StudentUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentInfoController {

    final StudentUseCase studentUseCase;

    @GetMapping("/list")
    ResponseEntity<StudentResponse> getList() {
        return studentUseCase.getList();
    }

    @PostMapping("/detail")
    ResponseEntity<StudentResponse> getDetail(@RequestBody StudentDetailRequest request) {
        return studentUseCase.getDetail(request);
    }
}
