package com.zulfikar.studentinfo.core.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentDetailRequest {
    String name;
}
