package com.zulfikar.studentinfo.core.useccases;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zulfikar.studentinfo.core.domain.StudentDetailRequest;
import com.zulfikar.studentinfo.core.domain.StudentResponse;
import com.zulfikar.studentinfo.data.db.entity.Student;
import com.zulfikar.studentinfo.data.db.repository.StudentRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentUseCase {
    final StudentRepo studentRepo;
    ObjectMapper objectMapper = new ObjectMapper();

    public ResponseEntity<StudentResponse> getList() {
        StudentResponse studentResponse = new StudentResponse();
        try {
            List<Student> students = studentRepo.findAll();
            studentResponse.setResponseCode("00");
            studentResponse.setResponseDesc("Success");
            studentResponse.setData(students);
            return ResponseEntity.status(HttpStatus.OK).body(studentResponse);
        } catch (Exception e) {
            log.error("caught Error :", e);
            studentResponse.setResponseCode("500");
            studentResponse.setResponseDesc("General Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(studentResponse);
        }
    }

    public ResponseEntity<StudentResponse> getDetail(StudentDetailRequest request) {
        StudentResponse studentResponse = new StudentResponse();
        String name = request.getName();
        try {
            List<Student> studentDetail = studentRepo.findByName(name);
            if (studentDetail == null || studentDetail.isEmpty()) {
                studentResponse.setResponseCode("404");
                studentResponse.setResponseDesc("Not Found Data With NAME " +name);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(studentResponse);
            }
            studentResponse.setResponseCode("00");
            studentResponse.setResponseDesc("Success");
            studentResponse.setData(studentDetail);
            return ResponseEntity.status(HttpStatus.OK).body(studentResponse);
        } catch (Exception e) {
            log.error("caught Error :", e);
            studentResponse.setResponseCode("500");
            studentResponse.setResponseDesc("General Error");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(studentResponse);
        }
    }
}
