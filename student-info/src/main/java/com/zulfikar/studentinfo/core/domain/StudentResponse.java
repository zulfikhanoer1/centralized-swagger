package com.zulfikar.studentinfo.core.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StudentResponse {
    String responseCode;
    String responseDesc;
    Object data;
}
