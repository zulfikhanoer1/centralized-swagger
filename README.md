# centralized swagger for school api

## How to Test App [PLAY WITH DOCKER]

``` diff
- Still Got This Error
```
![](./Capture.JPG?raw=true)

go to play with docker https://labs.play-with-docker.com/
- Create Sessions
- Create Instance
- Clone git ```git clone git@gitlab.com:zulfikhanoer1/centralized-swagger.git```
- change directory ```cd centralized-swagger```
- deploy app ```docker-compose up```
- expose port **8080** -> enter
- you'll be redirected to a page, add this path `/swagger-ui/html`
- you should have see a swagger ui that have services registered
![](./Screenshot_2024-02-26_080450.png?raw=true)



## How to Test App [LOCAL]

- install postgresql in local server & run query from `initsql\init.sql`
- run each services in this repo 
    - class-info
    - spring-eureka-server
    - student-info
    - swagger-gateway
- access `http://localhost:8080/swagger-ui.html`
- then you can change between services swagger and test it out using below table as reference
![](./Capture2.JPG)

## CLASS TABLE
| ID       | SUBJECT | GRADE |
| ------   | ------  | ----- |
|MATH-01   |MATH     |1      |
|BIOLOGY-01|BIOLOGY  |1      |
|MATH-02   |MATH     |2      |
|ART-01    |ART      |2      |

## STUDENT TABLE
| ID       | NAME | GRADE | ACTIVE_CLASS |
| ------   | ------  | ----- | ---------- |
|GENERATED |Samuel   |1      |{MATH-01,BIOLOGY-01}|
|GENERATED |Johnny   |1      |{MATH-01}|
|GENERATED |Fred     |2      |{MATH-01,MATH-02}|
|GENERATED |Solomon  |2      |{MATH-02,ART-02}|
